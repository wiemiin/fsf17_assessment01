"use strict";

var express = require("express");
var app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 4000;

app.use(express.static(__dirname + "/../client")); 

console.log(__dirname); //print out name of directory 
console.log(__dirname + "/../client"); 

app.post("/submit", function(req,res) {
    var result = req.body;
    console.log(result);
    res.status(200).json(result);
});

app.use(function (req, res) {
    res.send("<h1>Page Not Found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;