(function () {
    "use strict";
    angular.module("RegApp").controller("regCtrl", regCtrl);
    
    regCtrl.$inject = ["$http"];

    function regCtrl($http) {

        var self = this;

        self.user = {

        };

        self.onSubmit = onSubmit; 
        self.age = age; 

        self.empty = {
            dob: ""
        };

    function onSubmit() {

        $http.post("/submit", self.user)
        .then(function (result) {

            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.name);
            console.log(self.user.gender);
            console.log(self.user.dob);
            console.log(self.user.address);
            console.log(self.user.country);
            console.log(self.user.contact);

        }).catch(function(e){
            console.log(e);
        });

        self.showResult = true; 

    };

    function age() {

        var dob = self.user.dob;
        var year = dob.getFullYear();
        console.log(year);

        if (year <= 1999) {
            console.log("User is at least 18 years old");
            alert("You are above 18, please proceed with registration");
            
        } else {
            console.log("User is below 18 years old");
            alert("You must be at least 18 years old to proceed with registration");
            self.user.dob = null;
        } 
    }

    }

})();