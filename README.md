### SET UP ###

* git init 
* git remote add origin https://wiemiin@bitbucket.org/wiemiin/fsf17_assessment01.git
* mkdir server directory > app.js 
* mkdir client directory > app > app.module.js app.controller.js css > main.css
* create .gitignore (add node_modules and client/bower_components) 
* npm init (create package.json, enter entry point server/app.js)
* npm install express body-parser - - save  (root) 
* bower init (create package.json, enter entry point server/app.js)
* create .bowerrc (directory: "client/bower_components") 
* bower install bootstrap font-awesome angular - - save


### START SERVER ###

* go to fsf17_assessment01
* nodemon

### CLONE REPO ###

* git clone
* npm install
* bower install 